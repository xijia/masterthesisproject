﻿using Orleans.Configuration;
using Orleans.Hosting;
using System;
using System.Net;
using System.Threading.Tasks;

using Orleans;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;


namespace SiloHost
{
    class Silo
    {
        public static int Main(string[] args)
        {
            return RunMainAsync().Result;
        }

        private static async Task<int> RunMainAsync()
        {
            try
            {
                var host = await StartSilo();
                Console.WriteLine("Silo started!");
                Console.WriteLine("Press Enter to terminate...");
                Console.ReadLine();

                await host.StopAsync();

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return 1;
            }
        }

        private static async Task<ISiloHost> StartSilo()
        {
            // define the cluster configuration
            var builder = new SiloHostBuilder()
                .UseLocalhostClustering()
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "SimpleTests";
                })
                .Configure<EndpointOptions>(options => options.AdvertisedIPAddress = IPAddress.Loopback)
                //.AddMemoryGrainStorage("PubSubStore")
                //.AddSimpleMessageStreamProvider("ForTest")
                .ConfigureLogging(logging => logging.AddConsole());


            var host = builder.Build();
            await host.StartAsync();
            return host;
        }
    }
}

using GrainInterfaces;
using Orleans;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Grains
{
	public partial class SimpleEvents : Orleans.Grain, ISimpleEvents
	{
		private string username;
		public override Task OnActivateAsync()
		{
			username = this.GetPrimaryKeyString();
			return base.OnActivateAsync();
		}

		public Task<bool> apps_apply_for_access()
		{
			Thread.Sleep(20);
			return Task.FromResult(true);
		}
		public Task<bool> users_approve_image_access()
		{
			Thread.Sleep(20);
			return Task.FromResult(true);
		}
		public Task<bool> users_reject_image_access()
		{
			Thread.Sleep(20);
			return Task.FromResult(true);
		}
		public Task<bool> apps_access_image_file()
		{
			Thread.Sleep(20);
			return Task.FromResult(true);
		}
		public Task<bool> users_close_apps()
		{
			Thread.Sleep(20);
			return Task.FromResult(true);
		}
		public Task<bool> users_open_access_for_apps()
		{
			Thread.Sleep(20);
			return Task.FromResult(true);
		}
	}
}

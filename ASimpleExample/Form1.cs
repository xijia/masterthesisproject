﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrainInterfaces;
using Orleans;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ASimpleExample_External
{
	public partial class Form1 : Form
	{
		IClusterClient _client;
		
		private string username = "00000";
		public Form1(IClusterClient client)
		{
			InitializeComponent();
			Random random = new Random();
			int randomNumber = random.Next(10000, 99999);
			username = randomNumber.ToString();
			_client = client;
		}

		private async void _ui_ClickAsync(object sender, EventArgs e)
		{
            var appl = _client.GetGrain<ISimpleEvents>(username);
			var success = await appl.apps_access_image_file();
			if (success)
				MessageBox.Show("confirm",
									"confirm", MessageBoxButtons.OK,
									MessageBoxIcon.Information);
		}

		private async void apps_apply_for_access_ui_Click(object sender, EventArgs e)
		{
            var appl = _client.GetGrain<ISimpleEvents>(username);
			var success = await appl.apps_apply_for_access();
			if (success)
				MessageBox.Show("confirm",
									"confirm", MessageBoxButtons.OK,
									MessageBoxIcon.Information);

		}

		private async void users_approve_image_access_ui_Click(object sender, EventArgs e)
		{
			
            var appl = _client.GetGrain<ISimpleEvents>(username);
			var success = await appl.users_approve_image_access();
			if (success)
				MessageBox.Show("confirm",
									"confirm", MessageBoxButtons.OK,
									MessageBoxIcon.Information);
		}

		private async void users_reject_image_access_ui_Click(object sender, EventArgs e)
		{
            var appl = _client.GetGrain<ISimpleEvents>(username);
			var success = await appl.users_reject_image_access();
			if (success)
				MessageBox.Show("confirm",
									"confirm", MessageBoxButtons.OK,
									MessageBoxIcon.Information);
		}

		private async void users_close_apps_ui_Click(object sender, EventArgs e)
		{
            var appl = _client.GetGrain<ISimpleEvents>(username);
			var success = await appl.users_close_apps();
			if (success)
				MessageBox.Show("confirm",
									"confirm", MessageBoxButtons.OK,
									MessageBoxIcon.Information);
		}

		private async void users_open_access_for_apps_ui_Click(object sender, EventArgs e)
		{
            var appl = _client.GetGrain<ISimpleEvents>(username);
			var success = await appl.users_open_access_for_apps();
			if (success)
				MessageBox.Show("confirm",
									"confirm", MessageBoxButtons.OK,
									MessageBoxIcon.Information);
		}

	}
}

﻿namespace ASimpleExample_External
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.apps_apply_for_access_ui = new System.Windows.Forms.Button();
			this.users_approve_image_access_ui = new System.Windows.Forms.Button();
			this.users_reject_image_access_ui = new System.Windows.Forms.Button();
			this._ui = new System.Windows.Forms.Button();
			this.users_close_apps_ui = new System.Windows.Forms.Button();
			this.users_open_access_for_apps_ui = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// apps_apply_for_access_ui
			// 
			this.apps_apply_for_access_ui.Location = new System.Drawing.Point(30, 160);
			this.apps_apply_for_access_ui.Name = "apps_apply_for_access_ui";
			this.apps_apply_for_access_ui.Size = new System.Drawing.Size(120, 50);
			this.apps_apply_for_access_ui.TabIndex = 0;
			this.apps_apply_for_access_ui.Text = "apps apply for access";
			this.apps_apply_for_access_ui.UseVisualStyleBackColor = true;
			this.apps_apply_for_access_ui.Click += new System.EventHandler(this.apps_apply_for_access_ui_Click);
			// 
			// users_approve_image_access_ui
			// 
			this.users_approve_image_access_ui.Location = new System.Drawing.Point(169, 92);
			this.users_approve_image_access_ui.Name = "users_approve_image_access_ui";
			this.users_approve_image_access_ui.Size = new System.Drawing.Size(120, 55);
			this.users_approve_image_access_ui.TabIndex = 1;
			this.users_approve_image_access_ui.Text = "users approve image access";
			this.users_approve_image_access_ui.UseVisualStyleBackColor = true;
			this.users_approve_image_access_ui.Click += new System.EventHandler(this.users_approve_image_access_ui_Click);
			// 
			// users_reject_image_access_ui
			// 
			this.users_reject_image_access_ui.Location = new System.Drawing.Point(169, 218);
			this.users_reject_image_access_ui.Name = "users_reject_image_access_ui";
			this.users_reject_image_access_ui.Size = new System.Drawing.Size(120, 55);
			this.users_reject_image_access_ui.TabIndex = 2;
			this.users_reject_image_access_ui.Text = "users reject image access";
			this.users_reject_image_access_ui.UseVisualStyleBackColor = true;
			this.users_reject_image_access_ui.Click += new System.EventHandler(this.users_reject_image_access_ui_Click);
			// 
			// _ui
			// 
			this._ui.Location = new System.Drawing.Point(316, 150);
			this._ui.Name = "_ui";
			this._ui.Size = new System.Drawing.Size(110, 60);
			this._ui.TabIndex = 3;
			this._ui.Text = "apps access image file";
			this._ui.UseVisualStyleBackColor = true;
			this._ui.Click += new System.EventHandler(this._ui_ClickAsync);
			// 
			// users_close_apps_ui
			// 
			this.users_close_apps_ui.Location = new System.Drawing.Point(483, 92);
			this.users_close_apps_ui.Name = "users_close_apps_ui";
			this.users_close_apps_ui.Size = new System.Drawing.Size(116, 55);
			this.users_close_apps_ui.TabIndex = 4;
			this.users_close_apps_ui.Text = "users close apps";
			this.users_close_apps_ui.UseVisualStyleBackColor = true;
			this.users_close_apps_ui.Click += new System.EventHandler(this.users_close_apps_ui_Click);
			// 
			// users_open_access_for_apps_ui
			// 
			this.users_open_access_for_apps_ui.Location = new System.Drawing.Point(468, 217);
			this.users_open_access_for_apps_ui.Name = "users_open_access_for_apps_ui";
			this.users_open_access_for_apps_ui.Size = new System.Drawing.Size(147, 56);
			this.users_open_access_for_apps_ui.TabIndex = 5;
			this.users_open_access_for_apps_ui.Text = "users open access for apps";
			this.users_open_access_for_apps_ui.UseVisualStyleBackColor = true;
			this.users_open_access_for_apps_ui.Click += new System.EventHandler(this.users_open_access_for_apps_ui_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(686, 351);
			this.Controls.Add(this.users_open_access_for_apps_ui);
			this.Controls.Add(this.users_close_apps_ui);
			this.Controls.Add(this._ui);
			this.Controls.Add(this.users_reject_image_access_ui);
			this.Controls.Add(this.users_approve_image_access_ui);
			this.Controls.Add(this.apps_apply_for_access_ui);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button apps_apply_for_access_ui;
		private System.Windows.Forms.Button users_approve_image_access_ui;
		private System.Windows.Forms.Button users_reject_image_access_ui;
		private System.Windows.Forms.Button _ui;
		private System.Windows.Forms.Button users_close_apps_ui;
		private System.Windows.Forms.Button users_open_access_for_apps_ui;
	}
}


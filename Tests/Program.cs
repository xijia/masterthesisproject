﻿using GrainInterfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestLantencyAndThrouput
{
	class Program
	{
		static void _Main(string[] args)
		{
			try
			{
				var client = StartClient().GetAwaiter().GetResult();
				ThrouputSameUser(client);
				
				
				client.Close();
			}
			catch (Exception ee)
			{
				Console.WriteLine("Failed");
			}
		}


		public static async void LantencySameUser(IClusterClient client)
		{
			int count = 0;
			ISimpleEvents appl;
			appl = client.GetGrain<ISimpleEvents>("test");
			//单个运行的时间
			while (count < 200)
			{
				DateTime beforDT = System.DateTime.Now;

				await appl.apps_apply_for_access();
				DateTime afterDT = System.DateTime.Now;

				TimeSpan ts = afterDT.Subtract(beforDT);
				Console.WriteLine("Lantency: {0}ms.", ts.TotalMilliseconds);
				count++;
			}
			
		}

		public static async void LantencyDifferentUser(IClusterClient client)
		{
			int count = 0;
			//ISimpleEvents appl;
			//单个运行的时间
			DateTime beforDT = System.DateTime.Now;
			while (count <200)
			{
				var rand = new Random();
				var randnum = rand.Next(0, 1000000).ToString();
				var appl = client.GetGrain<ISimpleEvents>(randnum);
				//怎么测试这个？？
				await appl.apps_apply_for_access();
				count++;
			}
			DateTime afterDT = System.DateTime.Now;
			TimeSpan ts = afterDT.Subtract(beforDT);

			Console.WriteLine("Throuput: {0}ms/20.", ts.TotalMilliseconds);
		}


		public static void ThrouputDifferentUser2(IClusterClient client)
		{
			int count = 0;
			//ISimpleEvents appl;
			//单个运行的时间

			var tasks = new List<Task>();
			while (count < 300)
			{
				var rand = new Random();
				var randnum = rand.Next(0, 1000000).ToString();
				var appl = client.GetGrain<ISimpleEvents>(randnum);
				//怎么测试这个？？
				tasks.Add(appl.apps_apply_for_access());
				count++;
			}

			
			try
			{
				DateTime beforDT = System.DateTime.Now;
				Task t = Task.WhenAll(tasks);
				t.Wait();
				DateTime afterDT = System.DateTime.Now;
				TimeSpan ts = afterDT.Subtract(beforDT);
				Console.WriteLine("Throuput: {0}ms/300.", ts.TotalMilliseconds);
			}
			catch (Exception ee)
			{
				Console.WriteLine($"{ee}");
			}



		}

		public static void ThrouputSameUser(IClusterClient client)
		{
			int count = 0;
			ISimpleEvents appl;
			appl = client.GetGrain<ISimpleEvents>("test");
			//单个运行的时间
			
			try
			{
				DateTime beforDT = System.DateTime.Now;
				var tasks = new List<Task>();
				while (count < 1000)
				{
					tasks.Add(appl.apps_apply_for_access());
					count++;
				}
				Task t = Task.WhenAll(tasks);
				t.Wait();

				DateTime afterDT = System.DateTime.Now;
				TimeSpan ts = afterDT.Subtract(beforDT);

				Console.WriteLine("Throuput: {0}ms/1000.", ts.TotalMilliseconds);
			}
			catch (Exception ee)
			{
				Console.WriteLine($"{ee}");
			}

		}
		public static async void ThrouputDifferentUser(IClusterClient client)
		{
			int count = 0;
			ISimpleEvents appl;

			//单个运行的时间
			while (count < 200)
			{
				var rand = new Random();
				var randnum = rand.Next(0, 1000000).ToString();
				DateTime beforDT = System.DateTime.Now;
				appl = client.GetGrain<ISimpleEvents>(randnum);
				await appl.apps_apply_for_access();
				DateTime afterDT = System.DateTime.Now;

				TimeSpan ts = afterDT.Subtract(beforDT);
				Console.WriteLine("Lantency: {0}ms.", ts.TotalMilliseconds);
				count++;
			}

		}


		private static async Task<IClusterClient> StartClient()
		{


			var client = new ClientBuilder()
				//.AddOutgoingGrainCallFilter<OutGoingCallFilters>()
				.UseLocalhostClustering()
				.Configure<ClusterOptions>(options =>
				{
					options.ClusterId = "dev";
					options.ServiceId = "SimpleTests";
				})
				.ConfigureLogging(logging => logging.AddConsole())
				.Build();

			await client.Connect();
			return client;
		}
	}
}

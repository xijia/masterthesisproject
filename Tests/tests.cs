﻿using GrainInterfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace TestLantencyAndThrouput
{
	class tests
	{

		static Dictionary<Task, TimeSpan> taskTime = new Dictionary<Task, TimeSpan>();
		static List<TimeSpan> taskExecuteTime = new List<TimeSpan>();
		private class Configs
		{
			public int[] numOfEvents;
			public int interationsForTestThroughput;
			public int interationForSingleLatency;
		}

		static void __Main(string[] args)
		{
			StreamWriter sw = new StreamWriter(@"D:\MasterThesisProject\Tests_basic_new.txt");
			var configs = SetConfigs();
			var client = StartClient().GetAwaiter().GetResult();
			MultipleThroughput(client, sw, configs);
			MultipleThroughputDiffUser(client, sw, configs);

			multipleSameUserLatency(client, sw, configs);
			multipleDiffUserLatency(client, sw, configs);

			singleSameUserLatency(client, sw, configs);
			singleDiffUserLatency(client, sw, configs);
			client.Close();
			sw.Close();
		}
		
		static void Main(string[] args)
		{
			StreamWriter sw = new StreamWriter(@"D:\MasterThesisProject\newer\WhiteBoxTest_basic_1.txt");
			var configs = SetConfigs();
			var client = StartClient().GetAwaiter().GetResult();
			WhiteBoxTests(client, sw, @"D:\MasterThesisProject\WhiteBoxTest\testEvents.txt");
			/*testEventsNumsame(client, sw, configs);

			testEventsNumDiff(client, sw, configs);*/
			client.Close();
			sw.Close();
		}


		static void WhiteBoxTests(IClusterClient client, StreamWriter FinalResSw,
								   string EventsFilename)
		{
			
			var sr = new StreamReader(EventsFilename);

			string line;
			//var events = new List<string>();
			var appl = client.GetGrain<ISimpleEvents>("test");

			while ((line = sr.ReadLine()) != null)
			{
				var events = line.Split(",");
				for(int i= 0; i< events.Length-1; i++)
				{
					var e = events[i];
					try
					{
						executeEvents(appl, e);
					}
					catch
					{
						FinalResSw.WriteLine("false");
					}

				}
				FinalResSw.WriteLine("true");
			}
			//找到对应的函数
		
		}


		static void executeEvents(ISimpleEvents simpleEvents, string eventName)
		{
			switch (eventName)
			{
				case "apps_apply_for_access":
					simpleEvents.apps_apply_for_access();
					break;
				case "apps_access_image_file":
					simpleEvents.apps_access_image_file();
					break;
				case "users_approve_image_access":
					simpleEvents.users_approve_image_access();
					break;
				case "users_close_apps":
					simpleEvents.users_close_apps();
					break;
				case "users_open_access_for_apps":
					simpleEvents.users_open_access_for_apps();
					break;
				case "users_reject_image_access":
					simpleEvents.users_reject_image_access();
					break;

				default:
					throw new Exception("no arg");
			}
		}


		private static void CancelToken(Object obj)
		{
			Thread.Sleep(1000);
			Console.WriteLine("Canceling the cancellation token from thread {0}...",
							  Thread.CurrentThread.ManagedThreadId);
			CancellationTokenSource source = obj as CancellationTokenSource;
			if (source != null)
				source.Cancel();
		}

		private static async Task calculateLantencysame(IClusterClient client, StreamWriter sw,
														Configs configs, CancellationToken cancellation)
		{
			int count = 0;
			var tasks = new List<Task>();
			var timer = new Stopwatch();

			var appl = client.GetGrain<ISimpleEvents>("test");
			timer.Restart();
			while (count++ < 20)
			{

				var task = appl.apps_apply_for_access();
				taskTime[task] = timer.Elapsed;
				tasks.Add(task);
			}
			count = 0;
			while (count++ < 200000)
			{
				if (cancellation.IsCancellationRequested)
				{
					return;
				}
				var end = await Task.WhenAny(tasks.ToArray());
				taskExecuteTime.Add(timer.Elapsed - taskTime[end]);
				tasks.Remove(end);
				//debugs.Add(end);
				var tas = appl.apps_apply_for_access();
				tasks.Add(tas);
				taskTime[tas] = timer.Elapsed;

			}
		}




		private static async Task calculateLantencyDiff(IClusterClient client, StreamWriter sw,
														Configs configs, CancellationToken cancellation)
		{
			int count = 0;
			var tasks = new List<Task>();
			var timer = new Stopwatch();


			timer.Restart();
			while (count++ < 20)
			{

				var appl = client.GetGrain<ISimpleEvents>(count.ToString());
				var task = appl.apps_apply_for_access();
				taskTime[task] = timer.Elapsed;
				tasks.Add(task);
			}
			count = 0;
			while (count++ < 200000)
			{
				if (cancellation.IsCancellationRequested)
				{
					return;
				}
				var end = await Task.WhenAny(tasks.ToArray());
				taskExecuteTime.Add(timer.Elapsed - taskTime[end]);
				tasks.Remove(end);
				//debugs.Add(end);
				var appl = client.GetGrain<ISimpleEvents>((count + 20).ToString());
				var tas = appl.apps_apply_for_access();
				tasks.Add(tas);
				taskTime[tas] = timer.Elapsed;

			}
		}
		private static void testEventsNumDiff(IClusterClient client, StreamWriter sw, Configs configs)
		{
			CancellationTokenSource ts = new CancellationTokenSource();
			try
			{
				Console.WriteLine("start");
				Thread thread = new Thread(_ => CancelToken(ts));
				thread.Start();


				calculateLantencyDiff(client, sw, configs, ts.Token).Wait();
				double sum = 0;
				sw.WriteLine(taskExecuteTime.Count);
				foreach (var t in taskExecuteTime)
				{
					sum += t.TotalMilliseconds;
					sw.WriteLine(t.TotalMilliseconds);
				}
				var average = sum / taskExecuteTime.Count;
				sw.WriteLine(average);

				Console.WriteLine($"average: {average}");
				Console.WriteLine(taskExecuteTime.Count);

			}
			catch (OperationCanceledException ee)
			{
				//Console.WriteLine(taskExecuteTime.Count);
				double sum = 0;
				sw.WriteLine(taskExecuteTime.Count);
				foreach (var t in taskExecuteTime)
				{
					sum += t.TotalMilliseconds;
					sw.WriteLine(t.TotalMilliseconds);
				}
				var average = sum / taskExecuteTime.Count;

				Console.WriteLine($"average: {average}");
				Console.WriteLine(taskExecuteTime.Count);
				Console.WriteLine($"{ee}");
			}
		}


		private static void testEventsNumsame(IClusterClient client, StreamWriter sw, Configs configs)
		{
			CancellationTokenSource ts = new CancellationTokenSource();
			try
			{
				Console.WriteLine("start");
				Thread thread = new Thread(_ => CancelToken(ts));
				thread.Start();


				calculateLantencysame(client, sw, configs, ts.Token).Wait();
				double sum = 0;
				sw.WriteLine(taskExecuteTime.Count);
				foreach (var t in taskExecuteTime)
				{
					sum += t.TotalMilliseconds;
					sw.WriteLine(t.TotalMilliseconds);
				}
				var average = sum / taskExecuteTime.Count;
				sw.WriteLine(average);

				Console.WriteLine($"average: {average}");
				Console.WriteLine(taskExecuteTime.Count);
			}
			catch (OperationCanceledException ee)
			{
				//Console.WriteLine(taskExecuteTime.Count);
				double sum = 0;
				sw.WriteLine(taskExecuteTime.Count);
				foreach (var t in taskExecuteTime)
				{
					sum += t.TotalMilliseconds;
					sw.WriteLine(t.TotalMilliseconds);
				}
				var average = sum / taskExecuteTime.Count;
				sw.WriteLine(average);

				Console.WriteLine($"average: {average}");
				Console.WriteLine(taskExecuteTime.Count);
				Console.WriteLine($"{ee}");
			}
		}
		private static Configs SetConfigs()
		{
			var configs = new Configs();
			configs.numOfEvents = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 20, 25, 60, 100 };
			configs.interationsForTestThroughput = 5;
			configs.interationForSingleLatency = 100;
			return configs;
		}


		private static void multipleSameUserLatency(IClusterClient client, StreamWriter sw, Configs configs)
		{
			try
			{
				sw.WriteLine("multipleSameUserLatency");
				var numOfEvents = configs.numOfEvents;
				int iteration = configs.interationsForTestThroughput;
				/*				sw.WriteLine("iteration {0} times", iteration);
								sw.WriteLine(iteration);*/
				bool res = true;
				for (int i = 0; i < numOfEvents.Length && res; i++)
				{
					var num = numOfEvents[i];
					var iter = iteration;
					/*		sw.WriteLine("the number of events : {0}", num);
							sw.WriteLine(num);*/
					while (iter-- > 0)
					{
						res = HindLantencySameUser(client, sw, num);
					}
				}
			}
			catch (Exception ee)
			{
				Console.WriteLine("Failed, error: {0}", ee);
			}
		}

		public static bool HindLantencySameUser(IClusterClient client,
												StreamWriter sw,
												int numOfEvents)
		{
			try
			{
				int count = 0;
				ISimpleEvents appl;
				appl = client.GetGrain<ISimpleEvents>("test");
				var timer = new Stopwatch();
				var tasks = new List<Task>();
				var times = new List<TimeSpan>();
				timer.Restart();

				while (count++ < numOfEvents)
				{

					timer.Restart();
					var task = appl.apps_apply_for_access().ContinueWith(x =>
					{
						/*var endTime = timer.Elapsed;
						times.Add(endTime);*/
						timer.Stop();
						times.Add(timer.Elapsed);
					});
					tasks.Add(task);
				}



				Task.WaitAll(tasks.ToArray());
				foreach (var sth in tasks)
					if (sth.Status == TaskStatus.Faulted)
					{
						sw.WriteLine("Status Faulted:{0}", sth.Status);
						return false;
					}

				var latency = getAverageTimeSpan(times);
				//sw.WriteLine("{0}", latency);
				sw.WriteLine("hide latency: {0}ms/{1}.", latency, numOfEvents);
				return true;
			}
			catch (Exception ee)
			{
				sw.WriteLine($"{ee}");
				return false;
			}

		}


		private static void multipleDiffUserLatency(IClusterClient client, StreamWriter sw, Configs configs)
		{
			try
			{
				var numOfEvents = configs.numOfEvents;
				int iteration = configs.interationsForTestThroughput;
				/*				sw.WriteLine("iteration {0} times", iteration);
								sw.WriteLine(iteration);*/
				bool res = true;
				for (int i = 0; i < numOfEvents.Length && res; i++)
				{
					var num = numOfEvents[i];
					var iter = iteration;
					/*sw.WriteLine("the number of events : {0}", num);
					sw.WriteLine(num);*/
					while (iter-- > 0)
					{
						res = HindLantencyDiffUser(client, sw, num, iter.ToString());
					}
				}
			}
			catch (Exception ee)
			{
				Console.WriteLine("Failed, error: {0}", ee);
			}
		}

		public static bool HindLantencyDiffUser(IClusterClient client,
												StreamWriter sw,
												int numOfEvents,
												string userName)
		{
			try
			{
				int count = 0;
				//int count2 = 0;
				ISimpleEvents appl;

				var timer = new Stopwatch();
				var tasks = new List<Task>();
				var times = new List<TimeSpan>();
				timer.Restart();

				while (count++ < numOfEvents)
				{
					appl = client.GetGrain<ISimpleEvents>(userName + count.ToString());
					timer.Restart();
					var task = appl.apps_apply_for_access().ContinueWith(x =>
					{
						timer.Stop();
						times.Add(timer.Elapsed);
						/*						var endTime = timer.Elapsed;
												times.Add(endTime);*/
					});

					tasks.Add(task);
				}



				Task.WaitAll(tasks.ToArray());
				foreach (var sth in tasks)
					if (sth.Status == TaskStatus.Faulted)
					{
						/*sw.WriteLine("Status Faulted:{0}", sth.Status);*/
						return false;
					}

				var latency = getAverageTimeSpan(times);
				//sw.WriteLine("{0}", latency);
				sw.WriteLine("hide latency: {0}ms/{1}.", latency, numOfEvents);
				return true;
			}
			catch (Exception ee)
			{
				sw.WriteLine($"{ee}");
				return false;
			}

		}



		private static TimeSpan getAverageTimeSpan(List<TimeSpan> timeSpans)
		{
			TimeSpan sum = new TimeSpan(0);
			foreach (var t in timeSpans)
				sum += t;
			return sum / timeSpans.Count;

		}
		public static bool _HindLantencyDiffUser(IClusterClient client,
											StreamWriter sw,
											int numOfEvents,
											string userName)
		{
			//我咋觉得测试得不太对
			try
			{
				int count = 0;
				int count2 = 0;
				var allUser = new List<ISimpleEvents>();

				//单个运行的时间
				var timer = new Stopwatch();


				var tasks = new List<Task<bool>>();
				var recordTimes = new Dictionary<Task<bool>, TimeSpan>();
				while (count2++ < numOfEvents)
				{
					allUser.Add(client.GetGrain<ISimpleEvents>(userName + count.ToString()));
				}

				timer.Restart();

				while (count < numOfEvents)
				{
					//for easy test all monitor should inside
					/*timer.Stop();
					var startTime = timer.Elapsed;
					timer.Start();*/
					//timer.Restart();
					var startTime = timer.Elapsed;
					var task = allUser[count++].apps_apply_for_access();
					//添加进去的时间不算吗？？？？

					recordTimes.Add(task, startTime);
					tasks.Add(task);
				}
				//A task that represents the completion of one of the supplied tasks.
				//The return task's Result is the task that completed.
				//因为有可能第一个已经在循环还没跳出来的时候执行完了
				//但如果返回第一个的时间就不对
				var oneOfComplete = Task.WhenAny(tasks).GetAwaiter().GetResult();
				//timer.Stop();
				var endTime = timer.Elapsed;
				var latency = endTime - recordTimes[oneOfComplete];

				/*	sw.WriteLine("{0}", latency);
					sw.WriteLine("hide latency: {0}ms/{1}.", latency, numOfEvents);
	*/
				Task.WaitAll(tasks.ToArray());
				foreach (var sth in tasks)
					if (sth.Status == TaskStatus.Faulted)
					{
						sw.WriteLine("Status Faulted:{0}", sth.Status);
						return false;
					}
				return true;
			}
			catch (Exception ee)
			{
				sw.WriteLine($"{ee}");
				return false;
			}

			return true;
		}
		public static bool _HindLantencySameUser(IClusterClient client,
												StreamWriter sw,
												int numOfEvents)
		{
			//我咋觉得测试得不太对
			try
			{
				int count = 0;
				ISimpleEvents appl;
				appl = client.GetGrain<ISimpleEvents>("test");
				//单个运行的时间
				var timer = new Stopwatch();


				var tasks = new List<Task<bool>>();
				var recordTimes = new Dictionary<Task<bool>, TimeSpan>();
				timer.Restart();

				while (count++ < numOfEvents)
				{
					//for easy test all monitor should inside
					/*timer.Stop();
					var startTime = timer.Elapsed;
					timer.Start();*/
					//timer.Restart();
					var startTime = timer.Elapsed;
					var task = appl.apps_apply_for_access();
					//添加进去的时间不算吗？？？？

					recordTimes.Add(task, startTime);
					tasks.Add(task);
				}
				//A task that represents the completion of one of the supplied tasks.
				//The return task's Result is the task that completed.
				//因为有可能第一个已经在循环还没跳出来的时候执行完了
				//但如果返回第一个的时间就不对
				var oneOfComplete = Task.WhenAny(tasks).GetAwaiter().GetResult();
				//timer.Stop();
				var endTime = timer.Elapsed;
				var latency = endTime - recordTimes[oneOfComplete];

				/*				sw.WriteLine("{0}", latency);
								sw.WriteLine("hide latency: {0}ms/{1}.", latency, numOfEvents);*/

				Task.WaitAll(tasks.ToArray());
				foreach (var sth in tasks)
					if (sth.Status == TaskStatus.Faulted)
					{
						sw.WriteLine("Status Faulted:{0}", sth.Status);
						return false;
					}
				return true;
			}
			catch (Exception ee)
			{
				sw.WriteLine($"{ee}");
				return false;
			}

			return true;
		}

		private static void singleSameUserLatency(IClusterClient client, StreamWriter sw, Configs configs)
		{
			try
			{
				int iteration = configs.interationForSingleLatency;
				//sw.WriteLine("iteration {0} times", iteration);
				//sw.WriteLine(iteration);
				var num = 1;
				//sw.WriteLine("the number of events : {0}", num);
				//sw.WriteLine(num);
				var timer = new Stopwatch();
				var iter = 0;
				var times = new List<TimeSpan>();
				var appl = client.GetGrain<ISimpleEvents>("test");
				while (iter++ < iteration)
				{
					timer.Restart();
					var task = appl.apps_apply_for_access();
					task.Wait();
					timer.Stop();

					if (!task.Result)
						sw.WriteLine("sth wrong when test single latency");
					else
					{
						times.Add(timer.Elapsed);
						/*sw.WriteLine($"{timer.Elapsed}");
						sw.WriteLine($"Latency: { timer.Elapsed}/1.");*/
					}

				}

				sw.WriteLine("singleSameUserLatency average timespan {0}", getAverageTimeSpan(times));
			}
			catch (Exception ee)
			{
				Console.WriteLine("Failed, error: {0}", ee);
			}
		}



		private static void singleDiffUserLatency(IClusterClient client, StreamWriter sw,
												  Configs configs)
		{
			try
			{
				int iteration = configs.interationForSingleLatency;
				sw.WriteLine("iteration {0} times", iteration);
				sw.WriteLine(iteration);
				var num = 1;
				sw.WriteLine("the number of events : {0}", num);
				sw.WriteLine(num);
				var timer = new Stopwatch();
				var iter = 0;
				var iter2 = 0;

				var times = new List<TimeSpan>();
				var diffUsers = new List<ISimpleEvents>();
				while (iter2++ < iteration)
				{
					diffUsers.Add(client.GetGrain<ISimpleEvents>(iter2.ToString()));
				}

				while (iter < iteration)
				{
					timer.Restart();
					var task = diffUsers[iter++].apps_apply_for_access();
					task.Wait();
					timer.Stop();

					if (!task.Result)
						sw.WriteLine("sth wrong when test single latency");
					else
					{
						times.Add(timer.Elapsed);
						/*sw.WriteLine($"{timer.Elapsed}");
						sw.WriteLine($"Latency: { timer.Elapsed}/1.");*/
					}
				}
				sw.WriteLine("singleDiffUserLatency average timespan {0}", getAverageTimeSpan(times));
			}
			catch (Exception ee)
			{
				Console.WriteLine("Failed, error: {0}", ee);
			}
		}

		private static void MultipleThroughput(IClusterClient client, StreamWriter sw, Configs configs)
		{
			try
			{
				var numOfEvents = configs.numOfEvents;
				int iteration = configs.interationsForTestThroughput;
				/*sw.WriteLine("iteration {0} times", iteration);
				sw.WriteLine(iteration);*/
				bool res = true;
				for (int i = 0; i < numOfEvents.Length && res; i++)
				{
					var num = numOfEvents[i];
					var iter = iteration;
					/*sw.WriteLine("the number of events : {0}", num);
					sw.WriteLine(num);*/
					while (iter-- > 0)
					{
						res = ThrouputSameUser(client, sw, num);
					}
				}
			}
			catch (Exception ee)
			{
				Console.WriteLine("Failed, error: {0}", ee);
			}
		}



		public static bool ThrouputSameUser(IClusterClient client,
												StreamWriter sw,
												int numOfEvents)
		{
			int count = 0;
			ISimpleEvents appl;
			appl = client.GetGrain<ISimpleEvents>("test");
			//单个运行的时间
			var timer = new Stopwatch();

			try
			{
				var tasks = new List<Task<bool>>();

				timer.Restart();
				while (count++ < numOfEvents)
				{
					//for easy test all monitor should inside
					tasks.Add(appl.apps_apply_for_access());
				}
				Task.WaitAll(tasks.ToArray());
				timer.Stop();
				var ts = timer.Elapsed;
				//sw.WriteLine("{0}", ts);
				sw.WriteLine("Throuput: {0}ms/{1}.", ts, numOfEvents);


				foreach (var sth in tasks)
					if (sth.Status == TaskStatus.Faulted)
						sw.WriteLine("Status Faulted:{0}", sth.Status);

			}
			catch (Exception ee)
			{
				sw.WriteLine($"{ee}");
				return false;
			}

			return true;
		}

		private static void MultipleThroughputDiffUser(IClusterClient client, StreamWriter sw,
														Configs configs)
		{
			try
			{
				//sw.WriteLine("for different actors");
				var numOfEvents = configs.numOfEvents;
				int iteration = configs.interationsForTestThroughput;
				/*sw.WriteLine("iteration {0} times", iteration);
				sw.WriteLine(iteration);*/
				bool res = true;
				int count = 0;
				for (int i = 0; i < numOfEvents.Length && res; i++)
				{
					var num = numOfEvents[i];
					var iter = iteration;
					/*sw.WriteLine("the number of events : {0}", num);
					sw.WriteLine(num);*/
					while (iter-- > 0)
					{
						res = ThrouputDiffUser(client, sw, num, count.ToString());
						count++;
					}
				}
			}
			catch (Exception ee)
			{
				Console.WriteLine("Failed, error: {0}", ee);
			}
		}
		public static bool ThrouputDiffUser(IClusterClient client,
											StreamWriter sw,
											int numOfEvents,
											string userName)
		{
			int count = 0;
			int countForUsers = 0;

			var timer = new Stopwatch();
			var diffUsers = new List<ISimpleEvents>();

			try
			{
				var tasks = new List<Task<bool>>();

				while (countForUsers++ < numOfEvents)
				{
					diffUsers.Add(client.GetGrain<ISimpleEvents>(userName + countForUsers.ToString()));
				}

				timer.Restart();

				while (count < numOfEvents)
				{
					//for easy test all monitor should inside
					tasks.Add(diffUsers[count++].apps_apply_for_access());
				}
				Task.WaitAll(tasks.ToArray());
				timer.Stop();

				var ts = timer.Elapsed;
				//sw.WriteLine("{0}", ts);
				sw.WriteLine("Throuput: {0}ms/{1}.", ts, numOfEvents);


				foreach (var sth in tasks)
					if (sth.Status == TaskStatus.Faulted)
						sw.WriteLine("Status Faulted:{0}", sth.Status);
			}
			catch (Exception ee)
			{
				sw.WriteLine($"{ee}");
				return false;
			}

			return true;
		}

		private static async Task<IClusterClient> StartClient()
		{


			var client = new ClientBuilder()
				//.AddOutgoingGrainCallFilter<OutGoingCallFilters>()
				.UseLocalhostClustering()
				.Configure<ClusterOptions>(options =>
				{
					options.ClusterId = "dev";
					options.ServiceId = "SimpleTests";
				})
				.ConfigureLogging(logging => logging.AddConsole())
				.Build();

			await client.Connect();
			return client;
		}
	}
}

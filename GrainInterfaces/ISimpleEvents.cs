﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Orleans;
namespace GrainInterfaces
{
	public interface ISimpleEvents : IGrainWithStringKey
	{
		Task<bool> apps_apply_for_access();
		Task<bool> users_approve_image_access();
		Task<bool> users_reject_image_access();
		Task<bool> apps_access_image_file();
		Task<bool> users_close_apps();
		Task<bool> users_open_access_for_apps();
	}
}

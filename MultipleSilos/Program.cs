﻿using Orleans.Configuration;
using Orleans.Hosting;
using System;
using System.Net;
using System.Threading.Tasks;

using Orleans;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
namespace MultipleSilos
{
	class Program
	{
        private static async Task Main(string[] args)
        {
            int siloPort, gatewayPort;
            try
            {
                siloPort = int.Parse(args[0]);
                gatewayPort = int.Parse(args[1]);
            }
            catch (Exception)
            {
                siloPort = 11111;
                gatewayPort = 30000;
            }
            var invariant = "System.Data.SqlClient"; // for Microsoft SQL Server
            var connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Applications;
							Integrated Security=True;Pooling=False";
            var siloBuilder = new SiloHostBuilder()
                            // Clustering information
                            .Configure<ClusterOptions>(options =>
                            {
                                options.ClusterId = "dev";
                                options.ServiceId = "ServiceApp";
                            })
                            .UseAdoNetClustering(options =>
                            {
                                options.Invariant = invariant;
                                options.ConnectionString = connectionString;
                            })
                            .ConfigureEndpoints(siloPort: siloPort, gatewayPort: gatewayPort)

                            .ConfigureLogging(logging => logging.AddConsole());
            using (var host = siloBuilder.Build())
            {
                await host.StartAsync();
                Console.ReadLine();
            }
        }
    }
}

﻿using System;
namespace test
{
    public enum EventState { Include, Pending, Exclude};
    

    public class EventClass
    {
        public bool isExecuted;
        public string EventName;
        public EventState State;

        public EventClass(bool isExecuted, string EventName, EventState State)
        {
            this.isExecuted = isExecuted;
            this.EventName = EventName;
            this.State = State;
        }

    }
}



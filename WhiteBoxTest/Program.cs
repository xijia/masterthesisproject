﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Node = System.String;

namespace test
{
    class Program
    {

        public enum Arrow
        {
            Condition,
            Response,
            Include,
            Exclude,
            Milestone
        };


        public class Relation
        {
            public readonly string src;
            public readonly string tgt;
            public readonly Arrow rel;

            public Relation(string src, Arrow rel, string tgt)
            {
                this.src = src;
                this.tgt = tgt;
                this.rel = rel;
            }
        }


        public class EventsResult
        {
            //all events in the xml files
            public HashSet<string> eventNames;
            //the include events (these could be executed)
            public HashSet<string> included;
            public HashSet<string> executed;
            public HashSet<string> pending;
            public HashSet<Relation> relations;

            public EventsResult(HashSet<string> eventNames,
                                HashSet<string> executed,
                                HashSet<string> included,
                                HashSet<string> pending,
                                HashSet<Relation> relations)
            {
                this.eventNames = eventNames;
                this.executed = executed;
                this.included = included;
                this.pending = pending;
                this.relations = relations;
            }
        }

        private static void XMLReader(string file,
                                      out XmlDocument xmlDoc,
                                      out XmlReader reader)
        {
            xmlDoc = new XmlDocument();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true;
            reader = XmlReader.Create(file, settings);
            xmlDoc.Load(reader);
        }

        private static void GetEventsInGroups(XmlDocument xmlDoc,ref Dictionary<string,
                                                List<string>> allGroups, HashSet<string> eventNames)
        {
            foreach (XmlElement sth in xmlDoc.SelectNodes(
                         @"dcrgraph/specification/resources/events"))
            {
                if (!sth.GetType().Equals(typeof(XmlElement)))
                    continue;
                var events = (XmlElement)sth;
                if (events.Name.Equals("events"))
                {
                    List<string> NewEvents = new List<string>();
                    allGroups.Add("root", NewEvents);
                    getEvents(events, "root", ref allGroups, eventNames);
                }
            }

        }

        private static Relation findRelations(string src, string relations, string tgt)
        {
            //no spawn
            switch (relations)
            {
                case "conditions":
                    return new Relation(src, Arrow.Condition, tgt);
                case "responses":
                    return new Relation(src, Arrow.Response, tgt);
                case "excludes":
                    return new Relation(src, Arrow.Exclude, tgt);
                case "includes":
                    return new Relation(src, Arrow.Include, tgt);
                case "milestones":
                    return new Relation(src, Arrow.Milestone, tgt);
                default:
                    return null;

            }
        }

        private static void setupGroupEvents(Dictionary<string, List<string>> allGroups, HashSet<string> events, HashSet<string> eventNames, string id)
        {

            if (allGroups.ContainsKey(id))
                foreach (var _event in allGroups[id])
                    if (eventNames.Contains(_event))
                        events.Add(_event);
                    else
                        setupGroupEvents(allGroups, events, eventNames, _event);
            else
                events.Add(id);
        }

        private static void getEvents(XmlElement events, string parent,
                                ref Dictionary<string, List<string>> eventDic,
                                HashSet<string> eventNames)
        {

            
            if (events.ChildNodes.Count > 0)
            {
                foreach (var child in events.ChildNodes)
                {
                    if (!child.GetType().Equals(typeof(XmlElement)))
                        continue;
                    var xmlC = (XmlElement)child;

                    if (xmlC.Name.Equals("event"))
                    {
                        eventDic[parent].Add(xmlC.GetAttribute("id"));
                        if (xmlC.GetAttribute("type").Equals("nesting"))
                        {
                            List<string> NewEvents = new List<string>();
                            eventDic.Add(xmlC.GetAttribute("id"), NewEvents);
                            getEvents(xmlC, xmlC.GetAttribute("id"), ref eventDic, eventNames);

                        }
                        else
                        {
                            //nesting should be group, so the group name is not event
                            eventNames.Add(xmlC.GetAttribute("id"));
                        }

                    }
                }    
            }
        }

        private static void SetUpEvents(XmlDocument xmlDoc,
                                        Dictionary<string, List<string>> allGroups,
                                        HashSet<string> included, HashSet<string> executed,
                                        HashSet<string> pending, HashSet<string> eventNames)
        {
            var include = @"dcrgraph/runtime/marking/included";
            var execute = @"dcrgraph/runtime/marking/executed";
            var pend = @"dcrgraph/runtime/marking/pendingResponses";

            IntialEventsSet(include, xmlDoc, allGroups, included, eventNames);
            IntialEventsSet(execute, xmlDoc, allGroups, executed, eventNames);
            IntialEventsSet(pend, xmlDoc, allGroups, pending, eventNames);
        }

        private static void GetAllConstrains(XmlDocument xmlDoc, HashSet<Relation> relations)
        {
            foreach (XmlElement constraints in xmlDoc.SelectNodes(@"dcrgraph/specification/constraints"))//resources/events/event"))
            {
                foreach (XmlElement conditions in constraints.ChildNodes)
                {
                    foreach (XmlElement cdt in conditions.ChildNodes)
                    {

                        relations.Add(findRelations(cdt.GetAttribute("sourceId"),
                                      conditions.Name, cdt.GetAttribute("targetId")));
                    }
                }
            }
        }

        private static void IntialEventsSet(string nodeName, XmlDocument xmlDoc,
                                            Dictionary<string, List<string>> allGroups,
                                            HashSet<string> events, HashSet<string> eventNames)
        {
            foreach (XmlElement eventsSets in xmlDoc.SelectNodes(nodeName))
            {
                foreach (XmlElement eventid in eventsSets)
                {
                    var id = eventid.GetAttribute("id");
                    if (eventNames.Contains(id))
                        events.Add(id);
                }
            }
        }

        public static void addRelation(Dictionary<string, List<string>> dic,
                                        HashSet<Relation> relationsResults,
                                        String src, Arrow arr, String tgt)
        {
            if(dic.ContainsKey(src))
            {
                foreach (var e in dic[src])
                    addRelation(dic, relationsResults, e, arr, tgt);
            }
            else if(dic.ContainsKey(tgt))
            {
                foreach (var e in dic[tgt])
                    addRelation(dic, relationsResults, src, arr, e);
            }
            else
                relationsResults.Add(new Relation(src, arr, tgt));
        }


        public static HashSet<String> FirstEnableOnes(HashSet<String> included,
                                       HashSet<Relation> relations,
                                      HashSet<String> executed,
                                       HashSet<String> pending)
        {
            var result = new HashSet<string>(included);

            foreach (var r in relations)
            {
                switch (r.rel)
                {
                    case Arrow.Condition:
                        if (included.Contains(r.src) && !executed.Contains(r.src))
                            result.Remove(r.tgt);
                        break;

                    case Arrow.Milestone:
                        if (included.Contains(r.src) && pending.Contains(r.src))
                            result.Remove(r.tgt);
                        break;

                    default:
                        break;
                }
            }
            return result;

        }
            private static void GetGroupRelations(HashSet<Relation> relations,
                                         Dictionary<string, List<string>> dic,
                                          HashSet<Relation> relationsResults)
        {
            foreach (var relation in relations)
            {
                if (dic.ContainsKey(relation.src))
                    foreach (var funcName in dic[relation.src])
                        addRelation(dic,relationsResults, funcName, relation.rel, relation.tgt);
                else if (dic.ContainsKey(relation.tgt))
                    foreach (var funcName in dic[relation.tgt])
                        addRelation(dic,relationsResults, relation.src,
                                                relation.rel, funcName);
                else
                    addRelation(dic,relationsResults, relation.src, relation.rel, relation.tgt);
            }
        }

        static EventsResult xmlParser(string file)
        {
            XmlDocument xmlDoc;
            XmlReader reader;

            var allGroups = new Dictionary<string, List<string>>();
            var groupRelations = new HashSet<Relation>();

            var allRelations = new HashSet<Relation>();
            //all events in the xml files
            var eventNames = new HashSet<string>();
            //the include events (these could be executed)
            var included = new HashSet<string>();
            var executed = new HashSet<string>();
            var pending = new HashSet<string>();



            XMLReader(file, out xmlDoc, out reader);
            GetEventsInGroups(xmlDoc,ref allGroups, eventNames);
            SetUpEvents(xmlDoc, allGroups, included, executed, pending, eventNames);
            GetAllConstrains(xmlDoc, groupRelations);
            GetGroupRelations(groupRelations, allGroups, allRelations);
            included = FirstEnableOnes(included,allRelations,executed,pending);
            reader.Close();

            var res = new EventsResult(eventNames, executed, included, pending, allRelations);
            return res;
        }


        static List<List<string>> generateCorrespondingEvents(List<string> path)
        {
            long allOne = (long)Math.Pow(2, path.Count)-1;
            var res = new List<List<string>>();

            while (allOne != 0)
            {
                var events = generateEvents(allOne, path);
                res.Add(events);
                allOne--;
            }
            return res;
        }



        static List<string> generateEvents(long binary, List<string> events)
        {
            long endOne = 1;
            int count = 0;
            long res;
            var generateEvents = new List<string>();

            while (count < events.Count)
            {
                res = binary & endOne;
                if (res == 1)
                {
                    generateEvents.Add(events[count]);
                }
                count++;
                binary = binary >> 1;
            }
            return generateEvents;
        }


        void generateCorrespondingResults(EventsResult eventsResult)
        {
            foreach (var r in eventsResult.relations)
            {
                
            }
        }



        public class Edge
        {
            public Node StartPoint;
            public Node EndPoint;

            public Edge(Node A, Node B)
            {
                StartPoint = A;
                EndPoint = B;
            }

            public override bool Equals(object obj)
            {
                Edge e = obj as Edge;
                return this.StartPoint == e.StartPoint && this.EndPoint == e.EndPoint;
            }

            public override int GetHashCode()
            {
                return this.StartPoint.GetHashCode() * 100 + this.EndPoint.GetHashCode();
            }

        }

        public class Graph
        {
            public List<Edge> Edges;
            //public List<List<Edge>> Paths;
        }


        public static bool findCycle(List<Node> path, ref Edge lastEdge,
                                Node currentNode, Graph graph)
        {
            //有个问题 在其中任意一个环都会stack overflow
            path.Add(currentNode);
            foreach (var edge in graph.Edges)
            {
                if (edge.StartPoint == currentNode)
                {
                    if (path.Contains(edge.EndPoint))
                    {
                        lastEdge = edge;
                        path.Add(edge.EndPoint);
                        return true;
                    }

                    
                    //path.Add(edge.EndPoint);
                    if (findCycle(path, ref lastEdge,edge.EndPoint, graph))
                        return true;
                    
                    //break;
                }
            }
            path.Remove(currentNode);
            return false;
        }



        static void Main(string[] args)
        {

            Console.Write("!");
            var sw = new StreamWriter(@"/Users/xijia/Projects/parserXML/test/testEvents.txt");
            var swRes = new StreamWriter(@"/Users/xijia/Projects/parserXML/test/testEventsRes.txt");

            //var res = xmlParser(@"/Users/xijia/Downloads/TestFund.xml");
            var res = xmlParser(@"/Users/xijia/Downloads/Privacy.xml");
            //var events = generateCorrespondingEvents(res);

            var graph = new Graph();
            var rels = new HashSet<Edge>();

            foreach (var r in res.relations)
            {
                rels.Add(new Edge(r.src, r.tgt));
            }
            graph.Edges = rels.ToList();

            //然后一直找到没有圈为止
            var graphs = CreateGraphs(res, graph);

            var lstPath = new HashSet<string>();
            foreach (var g in graphs)
            {
                var paths = FindAllPath(g);
                ExtractPath(lstPath, paths);
            }

            foreach (var lstP in lstPath)
                sw.Write(lstP);
            sw.Close();

            //首先 初始化每个event 用EventClass
            //然后根据每个relation找到对应whiteTestClass
            //最后看final Check？
            

            var final = new List<bool>();
            foreach (var lstP in lstPath)
            {
                Dictionary<string, EventClass> allEvents = InitializeEvents(res);
                List<Relation> lstRel = GetCorrepondRelations(res, lstP);
                var tests = new List<WhiteTestClass>();
                foreach (var r in lstRel)
                {
                    allEvents[r.src].isExecuted = true;
                    allEvents[r.tgt].isExecuted = true;
                    switch (r.rel)
                    {
                        case Arrow.Condition:
                            tests.Add(new Condition(allEvents[r.src], allEvents[r.tgt]));
                            break;
                        case Arrow.Exclude:
                            tests.Add(new Exclude(allEvents[r.src], allEvents[r.tgt]));
                            break;
                        case Arrow.Include:
                            tests.Add(new Include(allEvents[r.src], allEvents[r.tgt]));
                            break;
                        case Arrow.Milestone:
                            tests.Add(new MileStone(allEvents[r.src], allEvents[r.tgt]));
                            break;
                        case Arrow.Response:
                            tests.Add(new Response(allEvents[r.src], allEvents[r.tgt]));
                            break;
                        default:
                            throw new Exception("cannot parse the condition");
                    }
                }

                //var listFinalRes = new List<bool>();
                var lastOne = true;
                foreach (var finalResult in tests)
                {
                    //listFinalRes.Add(finalResult._FinalTestResultForThisClass);
                    lastOne = lastOne & finalResult._FinalTestResultForThisClass;
                }

                Console.WriteLine(lastOne);
                final.Add(lastOne);
            }



            foreach (var f in final)
            {
                swRes.WriteLine(f);
            }
            swRes.Close();

        }

        private static Dictionary<string, EventClass> InitializeEvents(EventsResult res)
        {
            var allEvents = new Dictionary<string, EventClass>();
            //initializa the events
            foreach (var e in res.eventNames)
            {
                if (res.included.Contains(e))
                {
                    allEvents[e] = new EventClass(false, e, EventState.Include);
                }
                else if (res.pending.Contains(e))
                {
                    allEvents[e] = new EventClass(false, e, EventState.Pending);
                }
                else
                {
                    allEvents[e] = new EventClass(false, e, EventState.Exclude);
                }

            }

            return allEvents;
        }

        private static List<Relation> GetCorrepondRelations(EventsResult res, string lstPath)
        {
            var lstRel = new List<Relation>();
            
            var lsP = lstPath.Split(",");
            for (int s = 0; s < lsP.Count() - 1; s++)
            {
                foreach (var rls in res.relations)
                {
                    if (rls.src.Equals(lsP[s]) && rls.tgt.Equals(lsP[s + 1]))
                    {
                        lstRel.Add(rls);
                    }
                }
            }
            

            return lstRel;
        }



        private static void ExtractPath(HashSet<string> lstPath, List<List<Edge>> paths)
        {

            foreach (var p in paths)
            {
                var newPath = new List<string>();
                int i = 0;
                for (i = 0; i < p.Count(); i++)
                    newPath.Add(p[i].StartPoint);
                newPath.Add(p[i - 1].EndPoint);
                var pathForEvents = generateCorrespondingEvents(newPath);
                foreach (var pfe in pathForEvents)
                {
                    var newline = "";
                    for (int j = 0; j < pfe.Count(); j++)
                    {
                        var pf = pfe[j];
                        if (pf.Contains("_copy"))
                            pf = pf.Substring(0, pf.Count() - 5);
                        newline += (pf + ",");
                    }
                    newline += "\n";
                    lstPath.Add(newline);
                }
            }
        }

        static List<List<Edge>> FindAllPath(Graph graph)
        {
            var visited = new List<Edge>();
            var allPaths = new List<List<Edge>>();
            //改成拓扑图 不然都会遍历
            foreach (var e in graph.Edges)
            {
                var path = new List<Edge>();
                if (!visited.Contains(e))
                {
                    path.Add(e);
                    FindForward(graph, e, ref path);
                    FindBack(graph, e, ref path);
                }
                if (path.Count > 0)
                {
                    allPaths.Add(path);
                    foreach (var p in path)
                    {
                        Console.Write(p.StartPoint);
                        Console.Write(" " + p.EndPoint + " ");
                    }
                    Console.WriteLine();

                }
                foreach (var p in path)
                    visited.Add(p);
            }
            return allPaths;
        }



        static void FindBack(Graph graph, Edge edge, ref List<Edge> path)
        {
            for (int i = 0; i < graph.Edges.Count; i++)
            {
                var e = graph.Edges[i];
                //往后
                if (e.StartPoint == edge.EndPoint)
                {
                    path.Add(e);
                    FindBack(graph, e, ref path);
                    break;
                }
            }

        }


        static void FindForward(Graph graph, Edge edge,ref List<Edge> path)
        {
            for(int i = 0; i < graph.Edges.Count;i++)
            {
                var e = graph.Edges[i];
                //往前
                if (e.EndPoint == edge.StartPoint)
                {
                    path.Insert(0,e);
                    FindForward(graph, e, ref path);
                    break;
                }
             }

        }


        //然后一直找到没有圈为止
        static List<Graph> CreateGraphs(EventsResult res, Graph graph)
        {
            var needToChange = FindFinalEdges(res, graph);
            //重命名 或者放进某个数组里最后找对应的//应该改成node的
            var newGraphs = new List<Graph>();//应该有个数据结构变成新graphs里的paths

            foreach (var e in needToChange)
            {
                var newG = new Graph();
                newG.Edges = graph.Edges;
                for(int i = 0; i < newG.Edges.Count; i++)
                {
                    if (e == newG.Edges[i])
                    {
                        newG.Edges.Remove(e);
                        newG.Edges.Add(new Edge(e.StartPoint, e.EndPoint + "_copy"));
                        break;
                    }
                }
                newGraphs.Add(newG);
            }
            return newGraphs;
        }

        //找到圈最后一个连到头的线
        private static List<Edge> FindFinalEdges(EventsResult res, Graph graph)
        {
            var edges = new List<Edge>();
            foreach (var e in res.eventNames)
            {
                var path = new List<Node>();
                var edge = new Edge("null", "null");
                findCycle(path, ref edge, e, graph);

                
                if (path.Count > 0 && path[0] == path[path.Count - 1])
                    edges.Add(edge);

            }
            return edges;
        }

    }
}

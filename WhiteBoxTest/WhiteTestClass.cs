﻿using System;
namespace test
{
    public abstract class WhiteTestClass
    {
        public bool _FinalTestResultForThisClass;
        public EventClass _A;
        public EventClass _B;

        public WhiteTestClass(EventClass A, EventClass B )
        {
            _FinalTestResultForThisClass = true;
            _A = A;
            _B = B;

            if (CheckState())
                if (CheckConnection())
                    ChangeState();

            FinalCheck();

        }

        public void FinalCheck()
        {
            if (_B.isExecuted && _B.State == EventState.Exclude)
            {
                _FinalTestResultForThisClass = false;
                
            }
        }

        public bool CheckState()
        {
            if (_A.isExecuted && _A.State == EventState.Exclude)
            {
                _FinalTestResultForThisClass = false;
                return false;
            }

            return true;
        }

        public abstract bool CheckConnection();

        public abstract void ChangeState();

    }



    public class Condition : WhiteTestClass
    {
        public Condition(EventClass A, EventClass B) : base(A, B)
        {
            this._A = A;
            this._B = B;
        }

        public override bool CheckConnection()
        {

            if (!this._A.isExecuted && base._B.isExecuted)
            {
                this._FinalTestResultForThisClass = false;
                return false;
            }
            return true;
        }

        public override void ChangeState()
        {
            
        }
    }



    public class Response : WhiteTestClass
    {
        public Response(EventClass A, EventClass B) : base(A, B)
        {
            this._A = A;
            this._B = B;
        }

        public override bool CheckConnection()
        {
            if (_A.isExecuted)
                return true;

            return false;
        }

        public override void ChangeState()
        {
            this._B.State = EventState.Pending;
        }
    }


    public class MileStone : WhiteTestClass
    {
        public MileStone(EventClass A, EventClass B) : base(A, B)
        {
            this._A = A;
            this._B = B;
        }

        public override bool CheckConnection()
        {
            if (_A.State == EventState.Pending)
            {
                if(!_A.isExecuted && _B.isExecuted)
                {
                    this._FinalTestResultForThisClass = false;
                    return false;
                }
                
            }

            return true;
        }

        public override void ChangeState()
        {
            
        }
    }


    public class Include : WhiteTestClass
    {
        public Include(EventClass A, EventClass B) : base(A, B)
        {
            this._A = A;
            this._B = B;
        }

        public override bool CheckConnection()
        {
            if (!_A.isExecuted)
            {
                return false;
            }

            return true;
        }

        public override void ChangeState()
        {
            _B.State = EventState.Include;
        }
    }


    public class Exclude : WhiteTestClass
    {
        public Exclude(EventClass A, EventClass B) : base(A, B)
        {
            this._A = A;
            this._B = B;
        }

        public override bool CheckConnection()
        {
            if (!_A.isExecuted)
            {
                return false;
            }

            return true;
        }

        public override void ChangeState()
        {
            _B.State = EventState.Exclude;
        }
    }




}
